#ifndef MUESTRA_H
#define MUESTRA_H

/**
 *
 * @author nahuel
 */
#include "random.hpp"
#include <string>

using namespace std;

typedef class Muestra *muestra_t;

#define numDeMuestras 30

class Muestra {
private:
    random_t ran;
    float varAleatorio[numDeMuestras];
    int indec;

public:
    Muestra();
    Muestra(random_t ran);

    void up(int ind);
    float result();
    float X();
};

#endif                          /* MUESTRA_H */
