/* 
 * File:   serverListener.hpp
 * Author: nahuel
 *
 * Created on 19 de mayo de 2011, 15:50
 */

#ifndef SERVERLISTENER_H
#define	SERVERLISTENER_H


using namespace std;

typedef class ServerListener *serlistener_t;

class ServerListener{
protected:
public:
    ServerListener();
    float eval();
};

#endif	/* SERVERLISTENER_H */

