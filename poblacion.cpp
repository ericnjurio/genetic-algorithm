#include <iostream>
#include <fstream>
#include "poblacion.hpp"
using namespace std;

Poblacion::Poblacion(int sizePob) {
    numG = 0;
    path = "./poblacion-";
    this->sizePob = sizePob;
    radioMut = 0.01;
    radioCreep = 0.3;
    radioZap = 0.05;
    promiscuidad = .55;
    estatico = 0;
#ifdef SERVERLISTENER_H
    server = new ServerListener();
#endif
    ran = new Random();
    m = new Muestra(ran);
    poblacion = new cromosoma_t[sizePob];
    for (int i = 0; i < sizePob; i++) {
        poblacion[i] = new Cromosoma(ran, m);
    }
}

Poblacion::Poblacion(int sizePob, int estatico, float promiscuidad,
        float radioMut, float radioCreep, float radioZap) {
    numG = 0;
    path = "./poblacion-";
    this->sizePob = sizePob;
    this->radioMut = radioMut;
    this->radioCreep = radioCreep;
    this->radioZap = radioZap;
    this->promiscuidad = promiscuidad;
    this->estatico = estatico;
#ifdef SERVERLISTENER_H
    server = new ServerListener();
#endif
    ran = new Random();
    m = new Muestra(ran);
    poblacion = new cromosoma_t[sizePob];
    for (int i = 0; i < sizePob; i++) {
        poblacion[i] = new Cromosoma(ran, m);
    }
}

Poblacion::Poblacion(int sizePob, string pathToLoad) {
    gen_t Diploide[4][fenoLen];
    numG = 0;
    path = "./poblacion-";
    this->sizePob = sizePob;
    radioMut = 0.01;
    radioCreep = 0.3;
    radioZap = 0.05;
    promiscuidad = .55;
    estatico = 0;
#ifdef SERVERLISTENER_H
    server = new ServerListener();
#endif
    ran = new Random();
    m = new Muestra(ran);
    poblacion = new cromosoma_t[sizePob];
    ifstream in(pathToLoad.c_str());

    for (int i = 0; i < sizePob; i++) {
        //TODO: Implementar nueva
        in.read(reinterpret_cast<char*> (Diploide), 4 * fenoLen * sizeof (gen_t));
        //     poblacion[i] = new Cromosoma(ran, m, pathToLoad);

    }
}

Poblacion::~Poblacion() {
    if (poblacion) {
        for (int i = 0; i < sizePob; i++)
            if (poblacion[i]) {
                delete(poblacion[i]);
            }
        delete(poblacion);
    }
    delete(ran);
    delete(m);
#ifdef SERVERLISTENER_H
    delete(server);
#endif
}

int Poblacion::numGeneraciones() {
    return numG;
}

void Poblacion::save() {
    char buffer [80];
    string archivo; //        poblacion[i]->save(i);

    time_t now = time(0);
    // Convert now to tm struct for local timezone
    tm* localtm = localtime(&now);
    archivo.append(path);
    strftime(buffer, 80, "%Y-%m-%d_%H-%M-%S", localtm);
    archivo.append(buffer);

    ofstream fp_salida(archivo.c_str(), ios::binary);

    for (int i = 0; i < sizePob; i++) {
        fp_salida.write(reinterpret_cast<char*> (poblacion[i]->get_cadenaDiploide()),
                4 * fenoLen * sizeof (gen_t));
    }
    cout << "Poblacion guardada." << endl;
}

float Poblacion::evolucionar(int min_generaciones, int max_generaciones) {
    while (numG < min_generaciones || (.1 < evaluacion() && (numG < max_generaciones || max_generaciones < min_generaciones))) {
        // VER ACA SI NO EVOLUCIONA INCREMENTAR lOS CRUZAMIENTOS POR GENERACIONES
        //cout << numG << " " << poblacion[0]->get_edad() << " " << radioMut << " " << ((CromosomaImplementado) poblacion[0])->a << " " << ((CromosomaImplementado) poblacion[0])->b << " " << evaluacion() << endl;
        if (numG < min_generaciones) evaluacion();
        seleccion();
        sobrecruzamiento();
        evaluacion();
        /** Ver de hacer algun chiche con la mutacion... o de utilizar algun otro
         * operador como ZAP, Creep o algo así */
        mutacion();
        //remplazo();
        numG++;
        /**/
        cout << "Num Generacion: " << numG << endl;
        poblacion[0]->print();
        /**/
    }
    return evaluacion();
}

float Poblacion::evaluacion() {
    float res = 0.0;
    for (int i = 0; i < sizePob; i++) {
        for (int j = i; 0 < j && poblacion[j]->evaluacion() < poblacion[j - 1]->evaluacion(); j--) {
            swap(j - 1, j);
        }
        //poblacion[0]->print();
    }
    res = poblacion[0]->evaluacion();
    return res;
}

void Poblacion::mutacion() {
    for (int i = /** /1/**//**/0/**/; i < sizePob; i++) {
        //cout << endl << "Individuo " << i << endl;
        //poblacion[i]->print();
        /**/
        if (0 < i) {
            /**/
            poblacion[i]->mutar(radioMut);
            /**/
            poblacion[i]->zap(radioZap);
            /**/
        }/**/
        poblacion[i]->creep(radioCreep, i == 0);
        /**/
    }
}

void Poblacion::sobrecruzamiento() {
    if (1 < sizePob) {
        int i = 1;
        for (i = 0; i < (int) (((float) sizePob) * promiscuidad); i = i + 2) {
            cromosoma_t* c;
            /*cout << poblacion[i]->get_edad() << endl;
            cout << (poblacion[i]->mutar(radioMut)) << endl;
            cout << (poblacion[i] + poblacion[i + 1])[0]->get_edad() << endl;*/
            c = poblacion[i]->sobrecruzamiento(poblacion[i + 1], estatico);
            if (dHaming(c[0]->get_cadenaDiploide(), poblacion[i]->get_cadenaDiploide()) +
                    dHaming(c[1]->get_cadenaDiploide(), poblacion[i + 1]->get_cadenaDiploide()) <
                    dHaming(c[1]->get_cadenaDiploide(), poblacion[i]->get_cadenaDiploide()) +
                    dHaming(c[0]->get_cadenaDiploide(), poblacion[i + 1]->get_cadenaDiploide())) {
                if (c[0]->evaluacion() > poblacion[i]->evaluacion()) {
                    delete(c[0]);
                } else {
                    delete(poblacion[i]);
                    poblacion[i] = c[0];
                }
                if (c[1]->evaluacion() > poblacion[i + 1]->evaluacion()) {
                    delete(c[1]);
                } else {
                    delete(poblacion[i + 1]);
                    poblacion[i + 1] = c[1];
                }
            } else {
                if (c[1]->evaluacion() > poblacion[i]->evaluacion()) {
                    delete(c[1]);
                } else {
                    delete(poblacion[i]);
                    poblacion[i] = c[1];
                }
                if (c[0]->evaluacion() > poblacion[i + 1]->evaluacion()) {
                    delete(c[0]);
                } else {
                    delete(poblacion[i + 1]);
                    poblacion[i + 1] = c[0];
                }
            }
            delete(c);
        }
    }
}

void Poblacion::seleccion() {
    // Aca no deberia señeccionar a todos... solo a algunos para hacer sobrecruzamientos de esos...
    int i = 0;
    for (i = 0; i < (int) (((float) sizePob) * promiscuidad); i++) {
        // TODO: se podria ver de hacerlo de otra forma mas eficiente generando variables aleatorias de otras distribuciones
        if (ran->nextFloat()*((float) sizePob)<((float) i) + 1.0)
            swap(i, i + ran->nextInt(sizePob - i));
    }
}

void Poblacion::swap(int i, int j) {
    if (i != j && 0 <= i && 0 <= j && i < sizePob && j < sizePob) {
        cromosoma_t tmp = poblacion[i];
        poblacion[i] = poblacion[j];
        poblacion[j] = tmp;
    }
}

void Poblacion::print(int indiv) {
    if (indiv < sizePob) {
        cout << "Num Generacion: " << numG << endl;
        poblacion[indiv]->print();
    } else {
        cout << "La poblacion es mas chica de lo que esperas" << endl;
        cout << "sizePob: " << sizePob << endl;
    }
}