#ifndef RANDOM_H
#define RANDOM_H

typedef class Random *random_t;

class Random {
protected:
    long *sem;
public:
    Random();
    ~Random();
    int nextInt();
    int nextInt(int m);
    float nextFloat();
    float nextLFloat();
    unsigned long nextLong();
    unsigned long *semilla();
};

#endif                          /* RANDOM_H */
