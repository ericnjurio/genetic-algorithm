#include <math.h>
#include "muestra.hpp"

Muestra::Muestra() {
    ran = new Random();
    indec = 0;
    for (int i = 0; i < numDeMuestras; i++)
        varAleatorio[i] = ran->nextFloat()*200.0;
}

Muestra::Muestra(random_t ran) {
    for (int i = 0; i < numDeMuestras; i++)
        varAleatorio[i] = ran->nextFloat()*200.0;
    indec = 0;
    this->ran = ran;
}

void Muestra::up(int ind) {
    // throw new UnsupportedOperationException("Not yet implemented");
    indec = ind;
}

float Muestra::result() {
    return 159.52 * varAleatorio[indec] - 84.032;
    //Ver que para este caso anda... pero si lo usamos para otros no funca bien
    //return 159.52e4 * varAleatorio[indec] - 84.032e7;
}

float Muestra::X() {
    return varAleatorio[indec];
}
