/**
 *
 * @author nahuel
 */

#include <iostream>

#include "poblacion.hpp"

int fenoLen = getFenoLen();
// Ojo... aca se podria poner el ServerListener como global...

int main(int argc, char** argv) {
    int tamPoblac = 32;
    /**/
    poblacion_t poblacion = new Poblacion(tamPoblac);
    poblacion->evolucionar(10, 0);
    poblacion->print(0);
    //comentarlo mientras se hagan pruebas que no incluya salvar necesariamente
    //poblacion->save();
    delete(poblacion); /** /
    float x = -(0.0 / 0.0);
    long t = (*(long*) &(x)) - ((1 << 22) + 1); //4194305;
    cout << x << " " << *(long*) &(x) << endl;
    cout << *(float*) &(t) << " " << t << endl;
    cout << sizeof (int) << " " << sizeof (long) << " " << sizeof (float) << " " << sizeof (double) << endl;
    /**/
    return tamPoblac;
}
