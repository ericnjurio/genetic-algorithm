#ifndef POBLACION_H
#define POBLACION_H
/**
 *
 * @author nahuel
 */
#include <string>
#include "cromosoma.hpp"
#include "muestra.hpp"
#include "random.hpp"

using namespace std;

typedef class Poblacion *poblacion_t;

class Poblacion {
protected:
#ifdef SERVERLISTENER_H
    // Ver si combiene mas ponerlo como global arriba del main en evolucionador.c
    serlistener_t server;
#endif
    random_t ran;
    muestra_t m;
    cromosoma_t* poblacion;
    string path;
    int numG;
    int sizePob; // En este caso la poblacion no cambia de tamanio por aplicar Crowding Determinista
    /*int sizePNormal;
    int sizePActual;*/
    float promiscuidad;
    int estatico;
    float radioCreep;
    float radioZap;
    float radioMut; // Esto probablemente sea inecesario ... deberia estar en [.01,.005] o mas chico tal ves

    /*void remplazo();*/// No es necesario ya que el remplazo y evaluacion de cromosomas se realiza en el sobrecruzamiento
    void mutacion();
    void sobrecruzamiento();
    void seleccion();
    void swap(int i, int j);

public:
    Poblacion(int sizePob);
    Poblacion(int sizePob, string pathToLoad);
    Poblacion(int sizePob, int estatico, float promiscuidad, float radioMut,
            float radioCreep, float radioZap);
    ~Poblacion();
    int numGeneraciones();
    void save();
    void print(int indiv);
    float evolucionar(int min_generaciones, int max_generaciones);
    float evaluacion();
};

#endif                          /* POBLACION_H */
