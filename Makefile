TARGET=evolucionador
CC=g++
CFLAGS+= -ansi -Wall -Wextra -Wcast-qual -Wmissing-declarations -Wno-unused-parameter -Werror -pedantic -lm -pthread# -g `pkg-config --cflags glib-2.0`
#LDFLAGS=`pkg-config --libs glib-2.0`
SOURCESC=$(shell echo */*.c)
SOURCESCPP=$(shell echo *.cpp)
OBJECTSCPP=$(SOURCESCPP:.cpp=.o)
OBJECTSC=$(SOURCESC:.c=.o)

all: $(TARGET)
debug: CXX += -DDEBUG -g 
debug: CC += -DDEBUG -g 
debug: $(TARGET)

# BString requiere flags distintos:
$(BSTRING_OBJECTS):CFLAGS=-ansi -Wall -Werror -pedantic -g

$(TARGET): $(OBJECTSC) $(OBJECTSCPP)
	$(CC) $^ -o $@ $(CFLAGS)

clean:
	rm -f $(TARGET) $(OBJECTSC) $(OBJECTSCPP) $(BSTRING_OBJECTS) .depend *~ poblacion-* temp*
	make -C tests clean

test: $(OBJECTSC) $(OBJECTSCPP)
	make -C tests test

memtest: $(OBJECTSC) $(OBJECTSCPP)
	make -C tests memtest

.depend: *.[ch]
	$(CC) -MM $(SOURCESC) $(SOURCESCPP) >.depend

-include .depend

.PHONY: clean all

