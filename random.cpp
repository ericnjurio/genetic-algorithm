#include <limits.h>

#include "random.hpp"
#include "lib/generadores.h"
#include <iostream>
#include <ctime>
#include <math.h>

Random::Random() {
    sem = (long*) semilla();
    if (sem)
        if (*sem)
            *sem = -(*sem);
}

Random::~Random() {
    if (sem)
        delete(sem);
}

int Random::nextInt() {
    return ran2(sem) * INT_MAX;
}

int Random::nextInt(int m) {
    return ran2(sem) * m;
}

float Random::nextFloat() {
    return ran2(sem);
}

float Random::nextLFloat() {
    return (2.0 * ran2(sem) - 1.0)*(float) LONG_MAX;
}

unsigned long Random::nextLong() {
    return mzran13();
}

unsigned long* Random::semilla() {
    srand(time(NULL));
    unsigned long *i = new unsigned long;
    long res = 0, l, j;
    *i = rand() - 1;
    while (!res) {
        res = 1;
        l = (long) sqrt((double) (*i));
        j = 2;
        (*i)++;
        while (j <= l && res) {
            res = (*i) % j;
            if (j < 3)j = 3;
            else j += 2;
        }
    }
    //std::cout << "Encontre semilla " << (*i) << std::endl;
    return i;
}