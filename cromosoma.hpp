#ifndef CROMOSOMA_H
#define CROMOSOMA_H

#include "muestra.hpp"
#include "random.hpp"
#include "fenotipo.hpp"
#include <string>

using namespace std;

typedef class Cromosoma *cromosoma_t;

int dHaming(gen_t** a, gen_t** b);

class Cromosoma {
protected:
    random_t ran;
    muestra_t m;
    fenotipo_t fen;
    gen_t** cadenaDiploide;

public:
    Cromosoma(cromosoma_t c);
    Cromosoma(random_t ran, muestra_t m);
    Cromosoma(random_t ran, muestra_t m, gen_t** Diploide);
    ~Cromosoma();

    void mutar(float radioMut);
    void zap(float radioMut);
    void creep(float radioMut, int eval);
    float evaluacion();

    gen_t** get_cadenaDiploide() {
        return cadenaDiploide;
    }
    cromosoma_t *sobrecruzamiento(cromosoma_t pareja, int estatico);
    void print();
    //int save(int i);
};

#endif                          /* CROMOSOMA_H */
