#include "fenotipo.hpp"

#include <iostream>

int getFenoLen() {

    return 4;
}

/*
Fenotipo::Fenotipo(fenotipo_t f) {
   nota = 0.0;
   a = f->a;
   b = f->b;
   ran = f->ran;
   m = f->m;
   for (int i = 0; i < getFenoLen(); i++)
       cadenaGenotipica[i] = f->cadenaGenotipica[i];
}*/

Fenotipo::Fenotipo(random_t ran, muestra_t m, gen_t* cf) {
    nota = 0.0;
    this->ran = ran;
    this->m = m;
    /**
     * Esta es una buena opcion... pero deja sin efecto al acercamiento por Creep
     * /
    a = *(float*) &(cf[0]);
    //cout << cf[0] << endl;
    //cout << a << endl;
    b = *(float*) &(cf[1]);
    /**/
    /**
     * Esta es la otra opcion
     * /
    a = (float) cf[0] / (float) cf[1];
    b = (float) cf[2] / (float) cf[3];
    if (!isnormal(a)/** /a == NAN || a == INFINITY || a == -NAN || a == -INFINITY/** /) {
        a = (2.0 * ran->nextFloat() - 1.0)*(float) INT_MAX;
    }
    if (!isnormal(b)/** /b == NAN || b == INFINITY || b == -NAN || b == -INFINITY/** /) {
        b = (2.0 * ran->nextFloat() - 1.0)*(float) INT_MAX;
    }/**/
}

float absXD(float x) {
    if (x < 0.0) x = -x;
    return x;
}

float Fenotipo::evaluacionFenotipica() {
    float res = 0.0;
    /** /if (nota == 0.0) { /**/
    // esto es evolucionar a una recta por simulacion
    for (int i = 0; i < numDeMuestras; i++) {
        m->up(i);
        //res = res + absXD(eval(m->X()) - m->result());
        //res = res + abs(a - m->result() / m->X());
        //res = res + 1.0 / log(10.0 + abs(eval(m->X()) - m->result()));
    }
    //TODO: Ver que antes decia (res + 1) pero res era muy, muy chico 1x10^-28 + 1 = 1
    //res = 1.0 / (1.0 + res);
    //if (res < nota || nota == 0.0)
    nota = res;
    //else nota = (nota + res) / 2.0;
    /**/
    //nota = 1.0 / (1.0 + abs(pow(b, 2.0))); // esto es evolucionar a una cuadratica
    //nota = 1.0 / (1.0 + abs(b - 40.0));// esto es evolucionar a una recta con datos conosidos
    /** /}/**/
    return nota;
}

void Fenotipo::print() {
    cout << "ValAleatorio " << m->X() << endl;
    //cout << "Nota: " << nota << "\tA: " << a << "\tB: " << b << endl;
}