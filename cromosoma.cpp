/**
 *
 * @author nahuel
 */
#include <iostream>
#include <math.h>
#include "cromosoma.hpp"

Cromosoma::Cromosoma(cromosoma_t c) {
    //edad = 0;
    m = c->m;
    //    path = c->path;
    ran = c->ran;
    cadenaDiploide = new gen_t*[4];
    for (int i = 0; i < 4; i++) {
        cadenaDiploide[i] = new gen_t[fenoLen];
        for (int j = 0; j < fenoLen; j++)
            cadenaDiploide[i][j] = c->cadenaDiploide[i][j];
    }
    fen = NULL; //new Fenotipo(c->fen);
}

Cromosoma::Cromosoma(random_t ran, muestra_t m) {
    //edad = 0;
    //path = "";
    this->ran = ran;
    this->m = m;
    cadenaDiploide = new gen_t*[4];
    for (int i = 0; i < 4; i++) {
        cadenaDiploide[i] = new gen_t[fenoLen];
        for (int j = 0; j < fenoLen; j++) {
            cadenaDiploide[i][j] = ran->nextLong();
        }
    }
    fen = NULL;
}

Cromosoma::Cromosoma(random_t ran, muestra_t m, gen_t** Diploide) {
    //edad = 0;
    //TODO: recorrer el array
    //    path = pathToLoad;
    this->ran = ran;
    this->m = m;
    cadenaDiploide = new gen_t*[4];
    for (int i = 0; i < 4; i++) {
        cadenaDiploide[i] = new gen_t[fenoLen];
        for (int j = 0; j < fenoLen; j++)
            cadenaDiploide[i][j] = Diploide[i][j];
    }
    fen = NULL;
}

Cromosoma::~Cromosoma() {
    for (int i = 0; i < 4; i++)
        delete(cadenaDiploide[i]);
    delete(cadenaDiploide);
    delete(fen);
}

float Cromosoma::evaluacion() {
    if (fen == NULL) {
        gen_t cf[fenoLen];
        /**
         * x1d1 x2d2
         * (x1&x2)|((x1|x2)&(d1&d2))
         * (x1|((x1|x2)&(d1&d2)))&(x2|((x1|x2)&(d1&d2)))
         * ((x1|x2)&(x1|(d1&d2)))&((x1|x2)&(x2|(d1&d2)))
         * (x1|x2)&(x1|(d1&d2))&(x2|(d1&d2))
         * (x1|x2)&((x1&x2)|(d1&d2))
         */
        for (int i = 0; i < fenoLen; i++) {
            cf[i] = (cadenaDiploide[1][i] & cadenaDiploide[2][i]) |
                    ((cadenaDiploide[1][i] | cadenaDiploide[2][i]) &
                    cadenaDiploide[0][i] & cadenaDiploide[3][i]);
        }
        fen = new Fenotipo(ran, m, cf);
    }
    /* TODO: Clavar evaluacion */

    return fen->evaluacionFenotipica();
}

int dHaming(gen_t** a, gen_t** b) {
    int res = 0;
    int numBitsType = sizeof (gen_t) * 8;
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < fenoLen; j++) {
            gen_t t = (a[i][j])^(b[i][j]);
            for (int k = 0; k < numBitsType; k++) {
                t = t >> 1;
                res += t & 1;
            }
        }
    /** /
    if (res == 0) cout << "Hamming 0, fuck!!!!" << endl;
    else cout << "WIIII!!!! Hamiiinnng!!!!" << endl;
    /**/
    return res;
}

cromosoma_t* Cromosoma::sobrecruzamiento(cromosoma_t pareja, int estatico) {
    cromosoma_t* res = new cromosoma_t[2];
    gen_t **a = new gen_t*[4];
    for (int i = 0; i < 4; i++) {
        a[i] = new gen_t[fenoLen];
    }
    for (int j = 0; j < 2; j++) {
        for (int i = 0; i < fenoLen; i++) {
            gen_t r = ran->nextLong(), p[4];
            if (ran->nextInt(2) || estatico) {
                p[0] = pareja->cadenaDiploide[0][i];
                p[1] = pareja->cadenaDiploide[1][i];
                p[2] = pareja->cadenaDiploide[2][i];
                p[3] = pareja->cadenaDiploide[3][i];
            } else {
                p[0] = pareja->cadenaDiploide[3][i];
                p[1] = pareja->cadenaDiploide[2][i];
                p[2] = pareja->cadenaDiploide[1][i];
                p[3] = pareja->cadenaDiploide[0][i];
            }
            a[0][i] = (p[0] & (~r)) | (r & this->cadenaDiploide[0][i]);
            a[1][i] = (p[1] & (~r)) | (r & this->cadenaDiploide[1][i]);
            a[2][i] = (p[2] & r) | ((~r) & this->cadenaDiploide[2][i]);
            a[3][i] = (p[3] & r) | ((~r) & this->cadenaDiploide[3][i]);
        }
        res[j] = new Cromosoma(this->ran, this->m, a);
    }
    for (int i = 0; i < 4; i++) {
        delete(a[i]);
    }
    delete(a);
    return res;
}

void Cromosoma::mutar(float radioMut) {
    int change = 0;
    int numBitsType = sizeof (gen_t) * 8;
    int i = (int) (log(ran->nextFloat()) / log(1.0 - radioMut));
    while (i < fenoLen * 4 * numBitsType) {
        int k = i / numBitsType, b = i % numBitsType;
        cadenaDiploide[k / fenoLen][k % fenoLen] ^= 1 << b;
        change = 1;
        i += 1 + ((int) (log(ran->nextFloat()) / log(1.0 - radioMut)));
    }
    if (change && fen) {
        delete(fen);
        fen = NULL;
    }
}

void Cromosoma::zap(float radioMut) {
    int change = 0;
    int i = (int) (log(ran->nextFloat()) / log(1.0 - radioMut));
    while (i < fenoLen * 4) {
        cadenaDiploide[i / fenoLen][i % fenoLen] = ran->nextLong();
        change = 1;
        i += 1 + ((int) (log(ran->nextFloat()) / log(1.0 - radioMut)));
    }
    if (change && fen) {
        delete(fen);
        fen = NULL;
    }

}

void Cromosoma::creep(float radioMut, int eval) {
    int change = 0;
    if (eval) {
        cromosoma_t ctemp = new Cromosoma(this);
        ctemp->creep(radioMut, 0);
        if (ctemp->evaluacion() <= evaluacion()) {
            change = 1;
            for (int i = 0; i < 4; i++)
                for (int j = 0; j < fenoLen; j++)
                    cadenaDiploide[i][j] = ctemp->cadenaDiploide[i][j];
        }
        delete(ctemp);
    } else {
        int i = (int) (log(ran->nextFloat()) / log(1.0 - radioMut));
        while (i < fenoLen * 4) {
            if (i % 2)
                cadenaDiploide[i / fenoLen][i % fenoLen] += 1 /** /<< b/**/;
            else
                cadenaDiploide[i / fenoLen][i % fenoLen] -= 1 /** /<< b/**/;
            change = 1;
            i += 1 + ((int) (log(ran->nextFloat()) / log(1.0 - radioMut)));
        }
    }
    if (change && fen) {
        delete(fen);
        fen = NULL;
    }
}

void Cromosoma::print() {
    evaluacion();
    fen->print();
    for (int i = 0; i < 4; i++) {
        cout << "cadena" << i << ":";
        for (int j = 0; j < fenoLen; j++)
            cout << "\t" << cadenaDiploide[i][j];
        cout << endl;
    }
}

/*
int Cromosoma::save() {
    return 0;
}
 * */