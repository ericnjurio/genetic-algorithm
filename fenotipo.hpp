/* 
 * File:   fenotipo.hpp
 * Author: nahuel
 *
 * Created on 28 de abril de 2011, 22:01
 */

#ifndef FENOTIPO_H
#define	FENOTIPO_H

#include "muestra.hpp"
#include "random.hpp"
#include "serverListener.hpp"
/** /#include "RectaTest.hpp"/**/

typedef class Fenotipo *fenotipo_t;
/**
 * gen_t siempre tiene que ser de tipo int o long para poder usar los operadores
 * de bits... ojo al pasarlo a punto flotante sizeof(int)==sizeof(float) y
 * sizeof(long)==sizeof(double)
 */
typedef int gen_t;

extern int fenoLen;
int getFenoLen();

class Fenotipo : ServerListener {
protected:
    random_t ran;
    muestra_t m;
    float nota;
    gen_t* cadenaGenotipica;
public:
    //Fenotipo(fenotipo_t f);
    Fenotipo(random_t ran, muestra_t m, gen_t* cf);
    float evaluacionFenotipica();
    void print();
};

#endif	/* FENOTIPO_H */

